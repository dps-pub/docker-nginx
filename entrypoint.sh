#! /usr/bin/env sh


# get all of the varibles out of the env
# and split them on "=" to get the first half
# then join them with a space
varibles=$(env \
	| awk '{split($1,e,"="); print "$"e[1]}' \
	| tr '\n' ' ')

# Loop over every *.template file and make a *.conf from them
for f in /etc/nginx/conf.d/*.template
do
	envsubst "$varibles" < "$f" > "${f/\.template/.conf}"
done

exec "$@"