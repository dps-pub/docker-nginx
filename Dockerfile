FROM nginx:alpine

MAINTAINER Thomas Albrighton <tom@dps.com.au>

COPY entrypoint.sh /entrypoint.sh
COPY nginx.conf /etc/nginx/nginx.conf

VOLUME ["/var/www", "/etc/nginx/conf.d"]

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx"]